# buildConstructionForGiladSade


## Clone
```shell
$ git clone git@gitlab.com:ygalklein/buildconstructionforgiladsade.git
```
## Getting started

Put your f90 source files in the directory src/code_sources/

In the file `compilation/SystemsLibsLinks.py` edit the value of the keys `ifort` to the link of you intel mpif90 compiler, and do the same for the gfortran compiler in `gfortran` key. For the `lmod` key insert the link to your local operating system lmod command.

In the file `compilation/ModulesToLoad.py` edit the name of the module of the intel openmpi compiler.


## Compile
```shell
$ python3 -m compilation <config(s)>
```

`config` is a configuration the user wants - i.e which compiler, build type or parallelization mode.
The following allowed configurations are:
* `ifortReleaseSerial`
* `ifortDebugSerial`
* `ifortReleaseParallel`
* `ifortDebugParallel`
* `gnuReleaseSerial`
* `gnuDebugSerial`
* `gnuReleaseParallel`
* `gnuDebugParallel`

Running this command in your root directory would create a sub directory named `build` with a subdirectory with the name of `config` and if the compilation eneded successfully this directory would hold the binary file.

The head of the name of the binary that would be created can be edited in https://gitlab.com/ygalklein/buildconstructionforgiladsade/-/blob/035ebb5c06d89c119121d35f2e48fbaf85a2240e/compilation/_build.py#L60
