import argparse
import os
from . import build_program, root_dir

parser = argparse.ArgumentParser(prog="build")

parser.add_argument("--loglevel", default="info", choices=["error", "info", "debug"], help="The logging level to be used")

parser.add_argument("--make_dir", default=root_dir, help="The relative path to the build directory")

configurations = {"ifortReleaseSerial", "ifortDebugSerial", "gnuReleaseSerial", "gnuDebugSerial", "ifortReleaseParallel", "ifortDebugParallel", "gnuReleaseParallel", "gnuDebugParallel"}

def allowed_configuration(string):
    if "all_configurations" == string:
        return configurations
    configs = set(string.split(","))
    assert configs <= configurations, f'{string} is not a combination of elements from {" ".join(configurations)} separated by \",\" or \"all_configurations\"'
    return configs

parser.add_argument("configs",
                    type=allowed_configuration,
                    help="The desired build configurations",
                    metavar=f'config should be any \",\" separated combination of elements from {" ".join(configurations)} separated by \",\" or \"all_configurations\"')

args = parser.parse_args()

make_dir = os.path.abspath(args.make_dir)
src_dir = os.path.join(root_dir, 'src')

build_program(configs=args.configs,
                            make_dir=make_dir,
                            src_dir=src_dir,
                            loglevel=args.loglevel)