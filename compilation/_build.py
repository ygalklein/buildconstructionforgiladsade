#importing external packages
import logging
import os
import subprocess
import pathlib
import sys

# importing modules from this package
from .buildutils import lmod
from .buildutils import run_make

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger("build_program.main")

root_dir = str(pathlib.Path(__file__).parent.parent.absolute())
sys.path.append(root_dir)

def _run_cmake(*, build_dir, exe_name, config, SysLibsDict):

    if config.startswith("gnu"):
        fortran_compiler = SysLibsDict["gfortran"]
        cmake_fortran_flags = f" -fallow-argument-mismatch -ffree-line-length-none -cpp -fbacktrace -finit-local-zero "
        cmake_fortran_flags_debug = " -O0 -g -fbounds-check "
        cmake_fortran_flags_release = " -O2 "

    elif config.startswith("ifort"):
        fortran_compiler = SysLibsDict["ifort"]
        cmake_fortran_flags = f" -assume no2underscores -lstdc++ -lrt -traceback -init=zero -gen-interfaces -fpp "
        cmake_fortran_flags_debug = " -O0 -debug -check bounds "
        cmake_fortran_flags_release = " -O2 "

    else:
        logger.error(f"no known compiler was given at head of {config}")
    
    if "Release" in config:
        build_type = "Release"
    if "Debug" in config:
        build_type = "Debug"
    
    cmd = ['cmake',
            f'-DCMAKE_Fortran_COMPILER={fortran_compiler}',
            f'-DCMAKE_BUILD_TYPE={build_type}',
            f'-DCMAKE_Fortran_FLAGS={cmake_fortran_flags}',
            f'-DCMAKE_Fortran_FLAGS_DEBUG={cmake_fortran_flags_debug}',
            f'-DCMAKE_Fortran_FLAGS_RELEASE={cmake_fortran_flags_release}',
            f'-DISMIKBUL={"ON" if "Parallel" in config else "OFF"}',
            f'-DEXE_NAME={exe_name}',
            f'{build_dir}',
            f'{root_dir}/src']

    cmake_result = subprocess.run(cmd,
                                  stdout=open(os.path.join(build_dir, config+'_cmake.out'), 'w'),
                                  stderr=open(os.path.join(build_dir, config+'_cmake.err'), 'w'),
                                  cwd=os.path.join(build_dir, config))

    return cmake_result

def build_program(*, configs, make_dir, src_dir, loglevel="info"):
    """Build the program with the desired configurations."""
    exe_name = "projectName"
    logger.debug(f"args:\nconfigs = {configs}\nroot_dir = {root_dir}\nmake_dir = {make_dir}\nsrc_dir = {src_dir}\nloglevel = {loglevel}")
    logger.setLevel(getattr(logging, loglevel.upper()))

    assert os.path.isdir(os.path.join(root_dir, "src")), f"Directory {root_dir} does not contain a directory named src"

    with open(os.path.join(root_dir, "compilation", "SystemLibsLinks.py"), "r") as f:
        SysLibsDict = eval(f.read())
    assert "lmod" in SysLibsDict.keys(), f"Did not find lmod link in SysLibsDict"

    with open(os.path.join(root_dir, "compilation", "ModulesToLoad.py"), "r") as f:
        modules = eval(f.read())
    
    for m in modules:
        logger.info(f"loading module {m}")
        lmod.module(SysLibsDict["lmod"], "load", m)
    
    for config in configs:
        logger.info(f"Building {config}")
        build_dir = os.path.join(make_dir, "build")
        if not os.path.isdir(build_dir):
            os.makedirs(build_dir)
        config_dir = os.path.join(build_dir, config)
        if not os.path.isdir(config_dir):
            os.makedirs(config_dir)

        #run cmake for the specific config
        logger.info("Running cmake")
        cmake = _run_cmake(build_dir=build_dir,
                            exe_name=exe_name,
                            config=config,
                            SysLibsDict=SysLibsDict)
        assert cmake.returncode == 0, f"Running cmake for {config} failed"

        short_exe_path = os.path.join(config_dir, exe_name)
        if os.path.islink(short_exe_path):
            os.remove(short_exe_path)         

        #run make   
        logger.info("Running make")
        make = run_make.main(make_dir, config)
        assert make.returncode == 0, f"Running make for {config} failed"

        exe_suffix = "_"
        exe_suffix += config

        exe_path_with_suffix = os.path.join(config_dir, exe_name + exe_suffix)
        os.rename(short_exe_path, exe_path_with_suffix)
        os.symlink(exe_path_with_suffix, short_exe_path)
    



